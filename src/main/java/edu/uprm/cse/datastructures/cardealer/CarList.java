package edu.uprm.cse.datastructures.cardealer;

/*This class create a SortedList of cars, named carList.*/

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;


public class CarList {
	private static SortedList<Car> carList = new CircularSortedDoublyLinkedList<>(new CarComparator()); 
	
	private CarList(){
		
	}
	  
	/*The next method return a SortedList (carList).*/
	
	public static SortedList<Car> getInstance(){
		return carList;
		
	}

	/*The next method resets a SartedList to how is was originally.*/
	
	public static void resetCars() {
		
		carList = new CircularSortedDoublyLinkedList<>(new CarComparator()); 
	}
}