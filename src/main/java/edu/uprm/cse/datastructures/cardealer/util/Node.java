package edu.uprm.cse.datastructures.cardealer.util;

/*This class is using in the CircularDoublyLinkedList class.*/

public interface Node<E> {
	public E getElement();
	public void setElement(E e);

}