package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

/* This method compares obj1 with obj2 and returns either: 
 * a) 0 – if they are equal 
 * b) a negative number if obj1 is less than obj2 
   c) a positive number if obj1 is greater than obj2.*/

public class CarComparator implements Comparator<Car> {
	
	public int compare(Car obj1, Car obj2) {
		
				
	if(obj1.getCarBrand().compareTo(obj2.getCarBrand()) !=0){ 
		return obj1.getCarBrand().compareTo(obj2.getCarBrand()); //different brands
		}
	if(obj1.getCarModel().compareTo(obj2.getCarModel()) !=0) {
		return obj1.getCarModel().compareTo(obj2.getCarModel()); //same brand, different model
		}
	if(obj1.getCarModelOption().compareTo(obj2.getCarModelOption()) !=0) {
		return obj1.getCarModelOption().compareTo(obj2.getCarModelOption()); //same brand, same model, different options
		}
		return 0; // (equal) same car
	}			
}
