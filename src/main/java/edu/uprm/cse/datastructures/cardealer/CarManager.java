package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

                          /**REST API**/
/* The data objects are maintained and accessed in terms of four operations:
 *a)Create 
 *b)Read
 *c)Update
 *d)Delete
 */

@Path("/cars")
public class CarManager {

	private final  SortedList<Car> singleton = CarList.getInstance(); 
	
/*The next method returns either an array of cars.*/
	
	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] carsArray = new Car[singleton.size()];

		for (int i = 0; i < singleton.size(); i++) {
			carsArray[i] = singleton.get(i);
		}

		return carsArray;
	}

/*The next method returns the object created (car).*/
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){

		for (int i = 0; i < singleton.size(); i++) {
			if(singleton.get(i).getCarId()==id) {
				return singleton.get(i);
			}		
		}

		throw new NotFoundException();

	} 
	
/*The next method returns a status code indicating whether the object was added.*/
	
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		for (int i = 0; i < singleton.size(); i++) {
			if(singleton.get(i).getCarId() == car.getCarId())
				return Response.status(Response.Status.BAD_REQUEST).build();
		}

		if(singleton.add(car)) {
			return Response.status(201).build();
		}

		return Response.status(Response.Status.BAD_REQUEST).build();
	}

/*The next method returns the updated object.The data for the car comes 
 * in the payload of the PUT response, and a status code of 200 (OK). 
 * If the car is not found, then the PUT response shall be 404 (NotFound).*/
	
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(@PathParam("id") long id, Car car){
		if(car.getCarId() != id)
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();

		for(int i = 0; i < singleton.size(); i++) {
			if(singleton.get(i).getCarId() == car.getCarId()) {
				if(singleton.remove(singleton.get(i)) && singleton.add(car))
					return Response.status(Response.Status.OK).build();
			}
		}

		return Response.status(Response.Status.NOT_FOUND).build();

	}  
	
	
/* The next method simply returns a status code indicating whether the object 
 * was removed.If the car is not found, then the DELETE response shall be 
 * 404 (Not Found).*/
	
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		for(int i = 0; i < singleton.size(); i++) {
			if(singleton.get(i).getCarId() == id) {
				if(singleton.remove(singleton.get(i)))
					return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

}
