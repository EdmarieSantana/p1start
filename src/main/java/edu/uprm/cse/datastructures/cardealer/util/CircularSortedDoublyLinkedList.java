package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

  /* In this circular double linked list, the elements 
   * are kept in increasing sorted order based of the
   * comparator. The list also has a field to keep its 
   * current size. An empty list has size equal to 0, 
   * and one dummy header with its previous and next 
   * references pointing to itself.*/

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{
	
	private int size;
	private Node<E> header;
	private Comparator<E> comp;

	public CircularSortedDoublyLinkedList() {
		this.size = 0;
		this.header = new Node<E>();
		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.comp = new DefaultComparator();
	}
	

	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this();
		this.comp = comp;
	}

	@Override
	public Iterator<E> iterator() {
		return new CSDLLIterator();
	}

	/* The next method add the parameter in the list in increasing sorted 
	 * order based of the comparator and return true, if the object is 
	 * null the method returns false.*/
	
	@Override
	public boolean add(E obj) {
		if(obj.equals(null)) {
			return false;
		}

		Node<E> newNode = new Node<E>(obj, null, null);

		if(this.isEmpty()) {
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			newNode.setNext(this.header);
			newNode.setPrev(this.header);
			this.size++;
			return true;
		}


		Node<E> target = this.header.getNext();
		while(target != this.header) {

			if(comp.compare(obj, target.getElement()) <= 0) {
				newNode.setNext(target);
				newNode.setPrev(target.getPrev());
				target.setPrev(newNode);
				newNode.getPrev().setNext(newNode);
				this.size++;                                                   
				return true;
			}

			target = target.getNext();
		}

		newNode.setNext(this.header);
		newNode.setPrev(this.header.getPrev());
		this.header.setPrev(newNode);
		newNode.getPrev().setNext(newNode);
		this.size++;
		return true;
	}

	/*The next method returns the size of the list.*/
	
	@Override
	public int size() {
		return this.size;
	}

	/*The next method remove the object of the list and return true, if not remove 
	 * the object return false.*/
	
	@Override
	public boolean remove(E obj) {
		if(this.contains(obj)){
			return this.remove(this.firstIndex(obj));
		}
		return false;
	}

	/*The next method remove the object with the position of the
	 * parameter and return true.*/
	
	@Override
	public boolean remove(int index) {

		if ((index < 0) || (index >= this.size)){                       

			throw new IndexOutOfBoundsException();

		}
		Node<E> temp = this.header.getNext();
		for (int i = 0; i < index; i++) {
			temp = temp.getNext();
		}

		Node<E> prevTemp = temp.getPrev();
		Node<E> nextTemp = temp.getNext();
		prevTemp.setNext(nextTemp);
		nextTemp.setPrev(prevTemp);
		temp.setElement(null);
		temp.setNext(null);
		temp.setPrev(null);
		this.size--;

		return true;
	}

	/*The next method remove all objects equals to the parameter and returns 
	 * the number of the objects removed.*/
	
	@Override
	public int removeAll(E obj) {

		int count = 0;
		while (this.contains(obj)) {
			this.remove(obj);
			count++;
		}
		return count;
	}

	/*The next method returns the first object in the list.*/
	
	@Override
	public E first() {
		return this.header.getNext().getElement();
	}
	
	/*The next method returns the last object in the list.*/
	
	@Override
	public E last() {
		return this.header.getPrev().getElement();
	}
	
	/*The next method returns the object with the position of the
	 *parameter.*/

	@Override
	public E get(int index) {
		Node<E> temp = this.header.getNext();
		for (int i = 0; i < index; i++) {
			temp = temp.getNext();
		}

		return temp.getElement();
	}

	/*The next method empties the list.*/
	
	@Override
	public void clear() {

		Node<E> temp = header.getNext();
		while(temp != header) {
			Node<E> target = temp;
			temp = temp.getNext();
			target.setElement(null);
			target.setNext(null);
			target.setPrev(null);
		}

		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.size = 0;
	}
	
	/*The next method returns true if the list contains the parameter,
	 * if not return false.*/

	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;
	}

	/*The next method returns true if the list is empty and false if not.*/
	
	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	/*The next method returns the the first index of the object in the list,
	 * if not contains the object return -1.*/
	
	@Override
	public int firstIndex(E e) {
		int index = 0;

		Node<E> temp = this.header.getNext();
		while(temp != this.header) {
			if(temp.getElement().equals(e)) {
				return index;
			}
			temp = temp.getNext();
			index++;
		}

		return -1;
	}

	/*The next method returns the the last index of the object in the list,
	 * if not contains the object return -1.*/
	
	@Override
	public int lastIndex(E e) {
		int index = this.size-1;

		Node<E> temp = this.header.getPrev();
		while(temp != this.header) {
			if(temp.getElement().equals(e)){
				return index;
			}
			temp = temp.getPrev();
			index--;
		}

		return -1;
	}
	
	/*The next method set a comparator.*/
	
	public void setComparator(Comparator<E> comp) {
		this.comp = comp;
	}
	
	/* Each node has three references: 
	 * a) element – the object being stored
	 * b) next – a reference to the next node in the chain 
	 * c) prev – reference to the previous node.
	 *
	 */
	
	private static class Node<E>{
		private Node<E> prev;
		private Node<E> next;
		private E elem;

		public Node(E e, Node<E> prev, Node<E> next){
			this.elem = e;
			this.prev = prev;
			this.next = next;
		}

		public Node() {
			this(null, null, null);
		}

		public Node<E> getPrev(){
			return this.prev;
		}

		public Node<E> getNext(){
			return this.next;
		}

		public E getElement() {
			return this.elem;
		}

		public void setPrev(Node<E> node){
			this.prev = node;
		}

		public void setNext(Node<E> node){
			this.next = node;
		}

		public void setElement(E e) {
			this.elem = e;
		}
	}

	
	
	private class DefaultComparator implements Comparator<E>{
		
		/*The next method returns 1 if the first parameter if greater than the second, 
		 *and return -1 if is less.*/
		
		@Override
		public int compare(E o1, E o2) {
			String obj1 = o1.toString();
			String obj2 = o2.toString();
			return obj1.compareTo(obj2);
		}

	}

	private class CSDLLIterator implements Iterator<E>{
		private Node<E> currentNode;

		public CSDLLIterator() {
			this.currentNode = header.getNext();
		}
		
		/*The next method returns false if the current node is equals to header,
		 * and returns true if not is equals to header.*/
		
		@Override
		public boolean hasNext() {
			return this.currentNode != header;
		}

		@Override
		public E next() {
			if(!this.hasNext()) {
				throw new NoSuchElementException();
			}

			E result = currentNode.getElement();
			currentNode = currentNode.getNext();
			return result;
		}

	}
}